from django.http import response
from django.shortcuts import render
from django.http.response import HttpResponse
from django.core import serializers
from .models import Notes

def index(request):
    notes = Notes.objects.all()
    response = {'notes':notes}
    return render(request, 'lab2.html', response)

def xml(request):
    data = serializers.serialize('xml', Notes.objects.all())
    return HttpResponse(data, content_type="application/xml")

def json(request):
    data = serializers.serialize('json', Notes.objects.all())
    return HttpResponse(data, content_type="application/json")
