from django.urls import path
from .views import index, json, xml
from lab_2.models import Notes

urlpatterns = [
    path('', index, name='index'),
    path('xml', xml, name='xml'),
    path('json', json, name='json')
]