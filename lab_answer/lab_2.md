Muhammad Devin Pratista (2006485781)
PBP Lab 2

1. Apakah perbedaan antara JSON dan XML?

    Json merupakan format file menggunakan 'human readable' teks untuk menyimpan dan mentransmisi data objek yang mengandung atribut-value dan array.
    XML juga memiliki fungsi yang sama dengan Json yaitu untuk menyimpan sebuah koleksi data. Namun keduanya memiliki banyak perbedaan.
    Berikut merupakan perbedaan utama dari keduanya:
    - Objek pada Json memiliki sebuah type, sedangkan XML tidak
    - XML menyediakan namespace support, sedangkan Json tidak
    - XML memiliki kemampuan untuk menampilkan data, sedangkan Json tidak
    - Json tidak perlu menggunakan end tag, XML menggunakan start-end tag 
    - Tingkat keamanan Json tidak sekuat tingkat keamanan XML
    - Json hanya mendukung encoding dengan UTF-8, sedangkan XML mendukung encoding dengan berbagai macam format
    - Penyimpanan menggunakan Json dapat dilakukan dengan array, sedangkan XML tidak bisa
    - Json dapat diuraikan (dilakukan parsing) menjadi objek javascript yang siap digunakan, sedangkan melakukan parsing pada XML jauh lebih sulit dilakukan

    Referensi : - https://www.geeksforgeeks.org/difference-between-json-and-xml/
                - https://www.w3schools.com/js/js_json_xml.asp
                - https://www.guru99.com/json-vs-xml-difference.html

2. Apakah perbedaan antara HTML dan XML?

    HTML (Hyper Text Markup Language) merupakan markup language yang digunakan untuk membuat halaman dan aplikasi web.
    HTML tidak digunakan untuk mengangkut/mentransmisikan data, namun hanya untuk menampilkan data.
    XML (eXtension Markup Language) juga digunakan untuk membuat halaman dan aplikasi web, namun tidak untuk menampilkan data,
    melainkan untuk mengangkut/mentransmisikan data. Sehingga XML dikategorikan sebagai dinamis, sedangkan HTML sebagai statis.
    Terdapat beberapa perbedaan lainnya juga sebagai berikut:
    - HTML merupakan markup language, XML menyediakan framework untuk mendefinisikan markup language nya
    - XML berfokus pada pemindahan dan transmisi data, sedangkan HTML berfokus pada tampilan dari data
    - XML bersifat case sensitive, HTML tidak bersifat case sensitive
    - Tag pada HTML merupakan tag bawaan yang sudah terdefinisi, sedangkan XML didefinisikan oleh user
    - Tag pada XML dapat selalu diperbanyak, sedangkan tag pada HTML terbatas
    - XML menyediakan namespace support, HTML tidak
    - Closing tag pada XML sangat diperlukan, pada HTML tidak

    Referensi : - https://www.geeksforgeeks.org/html-vs-xml/
                - https://www.guru99.com/xml-vs-html-difference.html

