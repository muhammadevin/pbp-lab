from django.urls import path
from lab_2.models import Notes
from.views import add_notes, index, note_list

urlpatterns = [
    path('', index, name='index'),
    path('add-note', add_notes, name='add_notes'),
    path('note-list', note_list, name='note_list')
]