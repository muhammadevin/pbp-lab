from django import forms
from lab_2.models import Notes

class NotesForm(forms.ModelForm):

    class Meta:
        model = Notes
        fields = "__all__"