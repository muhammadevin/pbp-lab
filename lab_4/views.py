from django.http import response
from django.shortcuts import render
from django.http.response import HttpResponse, HttpResponseRedirect
from lab_4.forms import NotesForm
from lab_2.models import Notes

def index(request):
    notes = Notes.objects.all()
    response = {'notes':notes}
    return render(request, 'lab4_index.html', response)

def add_notes(request):
    response = {}
    form = NotesForm(request.POST or None)

    if (form.is_valid() and request.method == 'POST'):
        form.save()
        return HttpResponseRedirect('/lab-4')

    response['form'] = form
    return render(request, 'lab4_form.html', response)

def note_list(request):
    notes = Notes.objects.all()
    response = {'notes':notes}
    return render(request, 'lab4_note_list.html', response)