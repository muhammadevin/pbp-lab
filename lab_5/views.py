from django.http import response
from django.shortcuts import render
from django.http.response import HttpResponse
from django.core import serializers
from lab_2.models import Notes

def index(request):
    notes = Notes.objects.all()
    response = {'notes':notes}
    return render(request, 'lab5_index.html', response)