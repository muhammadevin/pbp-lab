from django.urls import path
from .views import index
from lab_2.models import Notes

urlpatterns = [
    path('', index, name='index'),
]